use rust_socketio::{Payload, SocketBuilder};
use serde_json::{json, Value};
use std::{thread, time};

fn main() {
    let mut socket = SocketBuilder::new("http://localhost:3000")
        .on("open", |_, _| println!("Connected"))
        .on("close", |_, _| println!("Disconnected"))
        .on("news", |payload: Payload, _| match payload {
            Payload::String(str) => {
                let json: Value = serde_json::from_str(&str).expect("Invalid JSON");
                println!(
                    "Incoming Company News email: {} {}",
                    json["title"], json["count"]
                );
            }
            Payload::Binary(_) => {}
        })
        .on("marketing", |payload: Payload, _| match payload {
            Payload::String(str) => {
                let json: Value = serde_json::from_str(&str).expect("Invalid JSON");
                println!(
                    "Incoming Marketing email: {} {}",
                    json["title"], json["count"]
                );
            }
            Payload::Binary(_) => {}
        })
        .connect()
        .expect("Connection failed");

    // sleep to wait for connection
    thread::sleep(time::Duration::from_secs(2));

    socket
        .emit("subscribe", json!({"event": "news"}))
        .expect("Server unreachable");
    socket
        .emit("subscribe", json!({"event": "marketing"}))
        .expect("Server unreachable");

    // loop to continue listening
    loop {
        thread::sleep(time::Duration::from_millis(100));
    }
}

use rust_socketio::SocketBuilder;
use serde_json::json;
use std::{thread, time};

fn main() {
    let mut socket = SocketBuilder::new("http://localhost:3000")
        .on("open", |_, _| println!("Connected"))
        .on("close", |_, _| println!("Disconnected"))
        .connect()
        .expect("Connection failed");

    // sleep to wait for connection
    thread::sleep(time::Duration::from_secs(2));

    let mut count = 0;

    loop {
        count += 1;

        socket
            .emit(
                "publish",
                json!({"event": "news", "args": {"title": "Company News", "count": count}}),
            )
            .expect("Server unreachable");

        thread::sleep(time::Duration::from_secs(5));
    }
}

const server = require('http').createServer();
const io = require('socket.io')(server);

io.on('connection', client => {
  console.log('Client', client.id, 'connected.');

  client.on('subscribe', ({event}) => {
    client.join(event);
    console.log('Subscriber', client.id, 'subscribed to event:', event);
  });

  client.on('publish', ({event, args}) => {
    console.log('Publisher', client.id, 'triggered event:', event, 'with args:', args);
    io.to(event).emit(event, args);
  });
  
});

server.listen(3000);
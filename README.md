# DASC TP4 Grupo 1

## Installing Dependencies (and Building Binaries)

### Node

`npm install` in _./broker_

### Rust

`cargo build` in _./pubsub_

## Starting Broker

`npm start` in _./broker_

## Starting Subscribers

### All Communications

`cargo run --bin subscriber-all` in _./pubsub_

### Marketing Communications Only

`cargo run --bin subscriber-marketing` in _./pubsub_

### Company News Communications Only

`cargo run --bin subscriber-news` in _./pubsub_

## Starting Publishers

### Marketing Communication

`cargo run --bin publisher-marketing` in _./pubsub_

### Company News Communications

`cargo run --bin publisher-news` in _./pubsub_
